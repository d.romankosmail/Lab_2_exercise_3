#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
using namespace std;
#define n 20 

int main()
{
	srand(time(0));		// äëÿ òðó ðàíäîìó
	setlocale(0, "ukr"); // óêð ìîâà
	int arr[n];
	int *mas = &arr[0];
	int *a = new int;
	int *b = new int;
	cout << "ââåä³òü a" << endl; cin >> *a;
	cout << "ââåä³òü b" << endl; cin >> *b;
	if (cin.fail() || *a > *b)  // ïåðåâ³ðêà íà ïðàâèëüíå ââåäåííÿ çíà÷åííü
	{
		cout << "íåêîðåêòíå ââåäåííÿ!" << endl;
		system("pause");
		return 0;
	}

	int i;
	int *min_index = new int;
	int *min_element_value = new int;
	int *sum = new int(0);
	int *first_id = new int(-1);
	int *second_id = new int(-1);
	for (i = 0; i < n; i++)
	{ 
		mas[i] = *a + rand() % (*b - *a + 1);		// çàäàºì çíà÷åííÿ åëåìåíòàì ìàñèâó
		// ïîøóê íîìåðó íàéìåíøîãî åëåìåíòó ìàñèâó
		if (i == 0) {*min_element_value = mas[0];}	// çàäàºì ïî÷àòêîâå çíà÷åííÿ çì³íí³é äëÿ íàéìåíøîãî çíà÷åííÿ
		cout << "¹" << i << ":" << mas[i] << endl;	// âèâäèì åëåìåíòè
		if (*min_element_value > mas[i])		// ïåðåâ³ðêà íà íàéìåíøå çíà÷åííÿ
		{
			*min_index = i;
			*min_element_value = mas[i];
		}
		//  âèçíà÷åííÿ äâîõ ïîñë³äîâíèõ â³ä'ºìíèõ åëåìíò³â ìàñèâó 
		if (*first_id == -1 && mas[i] < 0) 
		{
			*first_id = i;
		}
		if (*second_id == -1 && i > *first_id && mas[i] < 0)
		{
			*second_id = i;
		}
	}
	i = 0; // îáíóëÿºìî ë³÷èëüíèê
	cout << "íîìåð íàéìåíøîãî åëåìåíòà:" << *min_index << endl;
	cout << "íîìåð ïåðøîãî â³ä'ºìíîãî åëåìåíòó:" << *first_id << endl;
	cout << "íîìåð äðóãîãî â³ä'ºìíîãî åëåìåíòó:" << *second_id << endl;
	if ((*second_id - *first_id) > 1)
	{
		for (i = (*first_id + 1); i <= (*second_id - 1); i++)
		{
			*sum += mas[i];
		}
		cout << "Ñóìà åëåìåíò³â ìàñèâó, ðîçòàøîâàíèõ ì³æ ïåðøèì ³ äðóãèì â³ä'ºìíèìè åëåìåíòàìè: " << *sum << endl;
	}
	else cout << "ð³çíèöÿ ì³æ íîìåðàìè äâîõ ïîñë³äîâíèõ â³ä'ºìíèõ åëåìåíò³â º íåäîñòàòíüîþ äëÿ âèêîíàííÿ óìîâè" << endl;
	delete a, b, i, min_index, min_element_value, sum, first_id, second_id;
	system("pause");
	return 0;
}
